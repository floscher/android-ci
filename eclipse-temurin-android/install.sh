#!/bin/sh

# Loosely based on https://gitlab.com/gitlab-org/gitlab/-/blob/79603433a38b2e174ac6a9437fabafa1807f9e67/lib/gitlab/ci/templates/Android.gitlab-ci.yml

echo "# 1 # Upgrade and install APT packages."
apt-get --quiet update --yes
apt-get --quiet upgrade --yes
apt-get --quiet install --yes wget unzip xsltproc

echo "# 2 # Downloading and unpacking android commandline tools."
install -d "${ANDROID_HOME}"
wget --no-verbose --output-document="${ANDROID_HOME}/cmdline-tools.zip" "https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip"
unzip -q -d "${ANDROID_HOME}/cmdline-tools" "${ANDROID_HOME}/cmdline-tools.zip"
mv -T "${ANDROID_HOME}/cmdline-tools/cmdline-tools" "${ANDROID_HOME}/cmdline-tools/tools"

echo "# 3 # Installing packages with sdkmanager."
yes | sdkmanager --sdk_root="${ANDROID_HOME}" --licenses >/dev/null || true
sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"
sdkmanager "platform-tools"
sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"
